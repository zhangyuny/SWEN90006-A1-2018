package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  Machine m= new Machine();

  //Any method annotation with "@Test" is executed as a test.
  @Test public void Test1() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 1.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }

  @Test public void Test2() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 2.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test3() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 3.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test4() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 4.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test5() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 5.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test6() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 6.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test7() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 7.s");
	  m.execute(lines);
  }
  
  @Test public void Test8() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 8.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test9() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 9.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test10() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 10.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test11() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 11.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test12() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 12.s");
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test13() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 13.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test14() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 14.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test15() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 15.s");
	  m.execute(lines); 
	  throw new InvalidInstructionException();
  }
  
  @Test public void Test16() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC/EC 16.s");
	  m.execute(lines); 
  }
  
 
  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included


  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
