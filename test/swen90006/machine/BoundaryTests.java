package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
	Machine m= new Machine();
	@Test public void Test1() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B1.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }

	  @Test public void Test2() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B2.s");
		  m.execute(lines); 
		  
	  }
	  
	  @Test public void Test3() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B3.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test4() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B4.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test5() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B5.s");
		  m.execute(lines); 
		  
	  }
	  
	  @Test public void Test6() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B6.s");
		  m.execute(lines); 
		  
	  }
	  
	  @Test public void Test7() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B7.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test8() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B8.s");
		  m.execute(lines);
		  
	  }
	  
	  @Test public void Test9() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B9.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test10() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B10.s");
		  m.execute(lines);
		 
	  }
	  
	  @Test public void Test11() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B11.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test12() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B12.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test13() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B13.s");
		  m.execute(lines); 
	
	  }
	  
	  @Test public void Test14() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B14.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test15() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B15.s");
		  m.execute(lines); 
	
	  }
	  
	  @Test public void Test16() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B16.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test17() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B17.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test18() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B18.s");
		  m.execute(lines); 
		 
	  }
	  
	  @Test public void Test19() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B19.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test20() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B20.s");
		  m.execute(lines); 

	  }
	  
	  @Test public void Test21() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B21.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test22() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B22.s");
		  m.execute(lines);
	  }
	  
	  @Test public void Test23() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B23.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test24() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B24.s");
		  m.execute(lines); 
	  }
	  
	  @Test public void Test25() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B25.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test26() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B26.s");
		  m.execute(lines); 
	  }
	  
	  @Test public void Test27() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B27.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
	  @Test public void Test28() throws Throwable
	  {
		  final List<String> lines = readInstructions("examples/Boundary/B28.s");
		  m.execute(lines); 
		  throw new InvalidInstructionException();
	  }
	  
  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  @Test public void aFailedTest()
  {
    //include a message for better feedback
    final int expected = 2;
    final int actual = 1 + 2;
    assertEquals("Some failure message", expected, actual);
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
