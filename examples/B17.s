MOV R3 12               ; N = 12
MOV R2 1                ;
MOV R1 0                ; i = 0;
MOV R0 1                ; n = 1;
SUB R4 R3 R1            ; while(i != N)
JZ  R4 4                ; {
ADD R1 R1 R2            ;   i = i + 1;
MUL R0 R0 R1            ;   n = n * i;
JMP 11                  ; }
RET R0                  ; return n;
