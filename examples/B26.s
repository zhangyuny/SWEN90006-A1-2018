MOV R3 12               ; N = 12
MOV R2 1                ;
MOV R1 0                ; i = 0;
MOV R0 1                ; n = 1;
M0V R4 0            ; while(i != N)
JZ  R4 0                ; {
ADD R1 R1 R2            ;   i = i + 1;
MUL R0 R0 R1            ;   n = n * i;
JMP 2                  ; }
RET R0                  ; return n;
