MOV R4 -4              ; N = 12
MOV R2 1                ;

MOV R1 0                ; i = 0;
MOV R0 1                ; n = 1;

JZ  R4 4                ; {
ADD R1 R1 R2            ;   i = i + 1;
MUL R0 R0 R1            ;   n = n * i;
JMP -4                  ; }
RET R0                  ; return n;
